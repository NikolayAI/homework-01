import { Router } from 'express';
import { ioc } from '../inversify.config';
import { TestingController } from '../controllers/testing.controller';

const testingController = ioc.get(TestingController);

export const testingRouter = Router({});

testingRouter.delete('/all-data', testingController.allData.bind(testingController));