import { Router } from 'express';
import { handleFieldsErrors } from '../middlewares/field-validation/handle-fields-errors.middleware';
import { authMiddleware } from '../middlewares/auth.middleware';
import { validateUserId, validateUsers } from '../middlewares/field-validation/users-field-validation.middleware';
import { usersBodyValidator } from '../middlewares/body-validator/users-body-validator.middleware';
import { validatePostId } from '../middlewares/field-validation/posts-field-validation.middleware';
import { ioc } from '../inversify.config';
import { UsersController } from '../controllers/users.controller';

const usersController = ioc.get(UsersController);

export const usersRouter = Router({});

usersRouter.get('/', usersController.findAll.bind(usersController));

usersRouter.post(
  '/',
  authMiddleware(),
  usersBodyValidator(),
  validateUsers(),
  handleFieldsErrors(),
  usersController.create.bind(usersController),
);

usersRouter.get(
  '/:id',
  validatePostId(),
  handleFieldsErrors(),
  usersController.findOne.bind(usersController),
);

usersRouter.delete(
  '/:id',
  authMiddleware(),
  validateUserId(),
  handleFieldsErrors(),
  usersController.remove.bind(usersController),
);
