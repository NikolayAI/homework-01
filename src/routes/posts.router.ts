import { Router } from 'express';
import { validatePostId, validatePosts } from '../middlewares/field-validation/posts-field-validation.middleware';
import { handleFieldsErrors } from '../middlewares/field-validation/handle-fields-errors.middleware';
import { postsBodyValidator } from '../middlewares/body-validator/posts-body-validator.middleware';
import { authMiddleware } from '../middlewares/auth.middleware';
import { validateContent } from '../middlewares/field-validation/comments-field-validation.middleware';
import { authTokenMiddleware } from '../middlewares/auth-token.middleware';
import { ioc } from '../inversify.config';
import { PostsController } from '../controllers/posts.controller';

const postsController = ioc.get(PostsController);

export const postsRouter = Router({});

postsRouter.get('/', postsController.findAll.bind(postsController));

postsRouter.post(
  '/',
  authMiddleware(),
  postsBodyValidator(),
  validatePosts(),
  handleFieldsErrors(),
  postsController.create.bind(postsController),
);

postsRouter.get(
  '/:id',
  validatePostId(),
  handleFieldsErrors(),
  postsController.findOne.bind(postsController),
);

postsRouter.put(
  '/:id',
  authMiddleware(),
  postsBodyValidator(),
  validatePosts(),
  handleFieldsErrors(),
  postsController.update.bind(postsController),
);

postsRouter.delete(
  '/:id',
  authMiddleware(),
  validatePostId(),
  handleFieldsErrors(),
  postsController.remove.bind(postsController),
);

postsRouter.get(
  '/:id/comments',
  validatePostId(),
  handleFieldsErrors(),
  postsController.findAllComments.bind(postsController),
);

postsRouter.post(
  '/:id/comments',
  authTokenMiddleware(),
  validateContent(),
  handleFieldsErrors(),
  postsController.createComment.bind(postsController),
);