import { Router } from 'express';
import { validateVideoId, validateVideos } from '../middlewares/field-validation/videos-field-validation.middleware';
import { handleFieldsErrors } from '../middlewares/field-validation/handle-fields-errors.middleware';
import { videosBodyValidator } from '../middlewares/body-validator/videos-body-validator.middleware';
import { ioc } from '../inversify.config';
import { VideosController } from '../controllers/videos.controller';

const videosController = ioc.get(VideosController);

export const videosRouter = Router({});

videosRouter.get('/', videosController.findAll.bind(videosController));

videosRouter.post(
  '/',
  videosBodyValidator(),
  validateVideos(),
  handleFieldsErrors(),
  videosController.create.bind(videosController),
);

videosRouter.get(
  '/:id',
  validateVideoId(),
  handleFieldsErrors(),
  videosController.findOne.bind(videosController),
);

videosRouter.put(
  '/:id',
  videosBodyValidator(),
  validateVideos(),
  validateVideoId(),
  handleFieldsErrors(),
  videosController.update.bind(videosController),
);

videosRouter.delete(
  '/:id',
  validateVideoId(),
  handleFieldsErrors(),
  videosController.remove.bind(videosController),
);
