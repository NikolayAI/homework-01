import { Router } from 'express';
import { usersBodyValidator } from '../middlewares/body-validator/users-body-validator.middleware';
import {
  validateAuth,
  validateCode,
  validateEmail
} from '../middlewares/field-validation/auth-field-validation.middleware';
import { handleFieldsErrors } from '../middlewares/field-validation/handle-fields-errors.middleware';
import {
  registrationAttemptsValidatorMiddleware
} from '../middlewares/attempts-validator/registration-attempts-validator.middleware';
import {
  registrationConfirmationValidatorMiddleware
} from '../middlewares/attempts-validator/registration-confirmation-attempts-validator.middleware';
import {
  registrationEmailResendingValidatorMiddleware
} from '../middlewares/attempts-validator/registration-email-resending-attempts-validator.middleware';
import { authTokenMiddleware } from '../middlewares/auth-token.middleware';
import { ioc } from '../inversify.config';
import { AuthController } from '../controllers/auth.controller';

const authController = ioc.resolve(AuthController)

export const authRouter = Router({});

authRouter.get(
  '/me',
  authTokenMiddleware(),
  authController.me.bind(authController),
);

authRouter.post(
  '/registration',
  registrationAttemptsValidatorMiddleware(),
  validateAuth(),
  handleFieldsErrors(),
  authController.registration.bind(authController),
);

authRouter.post(
  '/registration-confirmation',
  registrationConfirmationValidatorMiddleware(),
  validateCode(),
  handleFieldsErrors(),
  authController.registrationConfirmation.bind(authController),
);

authRouter.post(
  '/registration-email-resending',
  registrationEmailResendingValidatorMiddleware(),
  validateEmail(),
  handleFieldsErrors(),
  authController.registrationEmailResending.bind(authController),
);

authRouter.post(
  '/login',
  // loginAttemptsValidatorMiddleware(),
  usersBodyValidator(),
  authController.login.bind(authController),
);

authRouter.post(
  '/refresh-token',
  // loginAttemptsValidatorMiddleware(),
  authController.refreshToken.bind(authController),
);

authRouter.post(
  '/logout',
  // loginAttemptsValidatorMiddleware(),
  authController.logout.bind(authController),
);
