import { Router } from 'express';
import {
  validateBloggerId,
  validateBloggers
} from '../middlewares/field-validation/bloggers-field-validation.middleware';
import { handleFieldsErrors } from '../middlewares/field-validation/handle-fields-errors.middleware';
import { bloggersBodyValidator } from '../middlewares/body-validator/bloggers-body-validator.middleware';
import { authMiddleware } from '../middlewares/auth.middleware';
import {
  validateContent,
  validateShortDescription,
  validateTitle
} from '../middlewares/field-validation/posts-field-validation.middleware';
import { ioc } from '../inversify.config';
import { BloggersController } from '../controllers/bloggers.controller';

const bloggersController = ioc.get(BloggersController);

export const blogsRouter = Router({});

blogsRouter.get('/', bloggersController.findAll.bind(bloggersController));

blogsRouter.post(
  '/',
  authMiddleware(),
  bloggersBodyValidator(),
  validateBloggers(),
  handleFieldsErrors(),
  bloggersController.create.bind(bloggersController),
);

blogsRouter.get(
  '/:id',
  validateBloggerId(),
  handleFieldsErrors(),
  bloggersController.findOne.bind(bloggersController),
);

blogsRouter.put(
  '/:id',
  authMiddleware(),
  bloggersBodyValidator(),
  validateBloggers(),
  validateBloggerId(),
  handleFieldsErrors(),
  bloggersController.update.bind(bloggersController),
);

blogsRouter.delete(
  '/:id',
  authMiddleware(),
  validateBloggerId(),
  handleFieldsErrors(),
  bloggersController.remove.bind(bloggersController),
);

blogsRouter.get(
  '/:id/posts',
  validateBloggerId(),
  handleFieldsErrors(),
  bloggersController.findAllPosts.bind(bloggersController),
);

blogsRouter.post(
  '/:id/posts',
  authMiddleware(),
  validateTitle(),
  validateShortDescription(),
  validateContent(),
  validateBloggerId(),
  handleFieldsErrors(),
  bloggersController.createPost.bind(bloggersController),
);
