import { Router } from 'express';
import {
  validateCommentId,
  validateComments
} from '../middlewares/field-validation/comments-field-validation.middleware';
import { handleFieldsErrors } from '../middlewares/field-validation/handle-fields-errors.middleware';
import { commentsBodyValidator } from '../middlewares/body-validator/comments-body-validator.middleware';
import { authTokenMiddleware } from '../middlewares/auth-token.middleware';
import { ioc } from '../inversify.config';
import { CommentsController } from '../controllers/comments.controller';

const commentsController = ioc.get(CommentsController);

export const commentsRouter = Router({});

commentsRouter.get(
  '/:id',
  validateCommentId(),
  handleFieldsErrors(),
  commentsController.findOne.bind(commentsController),
);

commentsRouter.put(
  '/:id',
  authTokenMiddleware(),
  validateCommentId(),
  commentsBodyValidator(),
  validateComments(),
  handleFieldsErrors(),
  commentsController.update.bind(commentsController),
);

commentsRouter.delete(
  '/:id',
  authTokenMiddleware(),
  validateCommentId(),
  handleFieldsErrors(),
  commentsController.remove.bind(commentsController),
);