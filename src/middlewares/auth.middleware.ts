import { NextFunction, Request, Response } from 'express';
import { base64decode } from 'nodejs-base64';

export const authMiddleware = () => {
  return (req: Request, res: Response, next: NextFunction) => {
    const credentials = 'Basic admin:qwerty'
    if (!req.headers.authorization) {
      return res.send(401);
    }
    if (req.headers.authorization.split(' ')[0] !== 'Basic') {
      return res.send(401);
    }
    if (credentials !== `Basic ${base64decode(req?.headers?.authorization.split(' ')[1])}`) {
      return res.send(401);
    }
    next();
  };
};