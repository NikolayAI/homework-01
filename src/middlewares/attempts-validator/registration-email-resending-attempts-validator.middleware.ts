import { NextFunction, Request, Response } from 'express';
import addSeconds from 'date-fns/addSeconds';

const registrationEmailResendingAttempts: Date[] = [];

export const registrationEmailResendingValidatorMiddleware = () => {
  return (req: Request, res: Response, next: NextFunction) => {
    registrationEmailResendingAttempts.push(new Date());

    if (registrationEmailResendingAttempts.length > 5) {
      const last6 = registrationEmailResendingAttempts.slice(-6);
      if (addSeconds(last6[0], 10) > last6[5]) {
        return res.sendStatus(429);
      }
    }

    next();
  };
};