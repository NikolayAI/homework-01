import { NextFunction, Request, Response } from 'express';
import addSeconds from 'date-fns/addSeconds';

const registrationAttempts: Date[] = [];

export const registrationAttemptsValidatorMiddleware = () => {
  return (req: Request, res: Response, next: NextFunction) => {
    registrationAttempts.push(new Date());

    if (registrationAttempts.length > 5) {
      const last6 = registrationAttempts.slice(-6);
      if (addSeconds(last6[0], 10) > last6[5]) {
        return res.sendStatus(429);
      }
    }

    next();
  };
};