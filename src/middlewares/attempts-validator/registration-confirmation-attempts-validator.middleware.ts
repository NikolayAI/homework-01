import { NextFunction, Request, Response } from 'express';
import addSeconds from 'date-fns/addSeconds';

const registrationConfirmationAttempts: Date[] = [];

export const registrationConfirmationValidatorMiddleware = () => {
  return (req: Request, res: Response, next: NextFunction) => {
    registrationConfirmationAttempts.push(new Date());

    if (registrationConfirmationAttempts.length > 5) {
      const last6 = registrationConfirmationAttempts.slice(-6);
      if (addSeconds(last6[0], 10) > last6[5]) {
        return res.sendStatus(429);
      }
    }

    next();
  };
};