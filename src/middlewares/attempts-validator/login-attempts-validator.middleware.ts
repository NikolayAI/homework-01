import { NextFunction, Request, Response } from 'express';
import addSeconds from 'date-fns/addSeconds';

const loginAttempts: Date[] = [];

export const loginAttemptsValidatorMiddleware = () => {
  return (req: Request, res: Response, next: NextFunction) => {
    loginAttempts.push(new Date());

    if (loginAttempts.length > 5) {
      const last6 = loginAttempts.slice(-6);
      if (addSeconds(last6[0], 10) > last6[5]) {
        return res.sendStatus(429);
      }
    }

    next();
  };
};