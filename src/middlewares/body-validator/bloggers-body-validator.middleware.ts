import { NextFunction, Request, Response } from 'express';

export const bloggersBodyValidator = () => {
  return (req: Request, res: Response, next: NextFunction) => {
    if (!req.body.name && !req.body.websiteUrl && !req.body.description) {
      return res.send(400);
    }
    next();
  };
};