import { NextFunction, Request, Response } from 'express';

export const usersBodyValidator = () => {
  return (req: Request, res: Response, next: NextFunction) => {
    if (!req.body.login && !req.body.password) {
      return res.send(400);
    }
    next();
  };
};