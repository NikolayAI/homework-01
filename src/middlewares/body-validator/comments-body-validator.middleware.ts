import { NextFunction, Request, Response } from 'express';

export const commentsBodyValidator = () => {
  return (req: Request, res: Response, next: NextFunction) => {
    if (!req.body.content) {
      return res.send(400);
    }
    next();
  };
};