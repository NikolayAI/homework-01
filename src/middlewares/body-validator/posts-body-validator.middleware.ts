import { NextFunction, Request, Response } from 'express';

export const postsBodyValidator = () => {
  return (req: Request, res: Response, next: NextFunction) => {
    if (
      !req.body.title &&
      !req.body.content &&
      !req.body.shortDescription &&
      !req.body.blogId
    ) {
      return res.send(400);
    }
    next();
  };
};