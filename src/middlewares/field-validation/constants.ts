export const POST_SHORT_DESCRIPTION_MAX_LENGTH = 100;
export const websiteUrlMatch = /^https:\/\/([a-zA-Z0-9_-]+\.)+[a-zA-Z0-9_-]+(\/[a-zA-Z0-9_-]+)*\/?$/
export const emailMatch = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/