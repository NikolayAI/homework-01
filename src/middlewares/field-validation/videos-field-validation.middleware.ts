import { body, param } from 'express-validator';

export const validateTitle = () => {
  return body('title')
    .isString()
    .notEmpty({ignore_whitespace: true})
    .isLength({ max: 40 })
    .withMessage('Video title should be valid.')
};
export const validateAuthor = () => {
  return body('author')
    .optional()
    .notEmpty({ignore_whitespace: true})
    .withMessage('Video author should be valid.')
};

export const validateVideoId = () => {
  return param('id')
    .notEmpty()
    .isString()
    .withMessage('Video id should be number that more than 0.');
};
export const validateVideos = () => {
  return [
    validateTitle(),
    validateAuthor(),
  ];
};
