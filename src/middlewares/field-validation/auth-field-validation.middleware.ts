import { body } from 'express-validator';
import { emailMatch } from './constants';
import { ioc } from '../../inversify.config';
import { UsersRepository } from '../../repositories/users.repository';

const usersRepository = ioc.get(UsersRepository);

export const validateLogin = () => {
  return body('login')
    .isString()
    .notEmpty({ ignore_whitespace: true })
    .isLength({ min: 3 })
    .isLength({ max: 10 })
    .withMessage('User login should be valid.')
    .custom(async login => {
      const user = await usersRepository.findUsersAccountByLoginOrEmailOrPasswordHash({ login });
      if (user) throw new Error('User is already exist');
    });
};

export const validateEmailWithMatchExistingAccount = () => {
  return body('email')
    .matches(emailMatch)
    .withMessage('email should be valid.')
    .custom(async email => {
      const user = await usersRepository.findUsersAccountByLoginOrEmailOrPasswordHash({ email });
      if (user) throw new Error('User is already exist');
    });
};

export const validateEmail = () => {
  return body('email')
    .matches(emailMatch)
    .withMessage('email should be valid.')
    .custom(async email => {
      const user = await usersRepository.findUsersAccountByLoginOrEmailOrPasswordHash({ email });
      if (!user) throw new Error('Incorrect email');
      if (user.emailConfirmation.isConfirmed) {
        throw new Error('Email is already confirmed');
      }
    });
};

export const validatePassword = () => {
  return body('password')
    .isString()
    .notEmpty({ ignore_whitespace: true })
    .isLength({ min: 6 })
    .isLength({ max: 20 })
    .withMessage('User password should be valid.');
  // .custom(async password => {
  //   const passwordHash = await authService._generateHash(password);
  //   const user = await usersRepository.findUsersAccountByLoginOrEmailOrPasswordHash({ passwordHash });
  //   if (user) throw new Error('User is already exist');
  // })
};

export const validateCode = () => {
  return body('code')
    .isString()
    .custom(async code => {
      const user = await usersRepository.findByConfirmationCode({ code });
      if (!user) throw new Error('Incorrect code');
      if (user.emailConfirmation.isConfirmed) throw new Error('Code is already confirmed');
    });
};

export const validateAuth = () => {
  return [
    validateLogin(),
    validateEmailWithMatchExistingAccount(),
    validatePassword(),
  ];
};
