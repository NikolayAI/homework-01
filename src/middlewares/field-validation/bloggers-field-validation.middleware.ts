import { body, param } from 'express-validator';
import { websiteUrlMatch } from './constants';

export const validateName = () => {
  return body('name')
    .isString()
    .notEmpty({ignore_whitespace: true})
    .isLength({ min: 0 })
    .isLength({ max: 15 })
    .withMessage('Blogger name should be valid.')
    // .withMessage('Blogger name should contains more than 1 symbol.')
    // .withMessage('Blogger name should contains less than 15 symbols.')
};
export const validateDescription = () => {
  return body('description')
    .isString()
    .notEmpty({ignore_whitespace: true})
    .isLength({ min: 0 })
    .isLength({ max: 500 })
    .withMessage('Blog description should be valid.')
    // .withMessage('Blogger name should contains more than 1 symbol.')
    // .withMessage('Blogger name should contains less than 15 symbols.')
};
export const validateWebsiteUrl = () => {
  return body('websiteUrl')
    .matches(websiteUrlMatch)
    .isLength({ max: 100 })
    .withMessage('WebsiteUrl url should be valid.')
};

export const validateBloggerId = () => {
  return param('id')
    .notEmpty()
    .isString()
    .withMessage('Blogger id should be number that more than 0.');
};
export const validateBloggers = () => {
  return [
    validateName(),
    validateWebsiteUrl(),
    validateDescription(),
  ];
};
