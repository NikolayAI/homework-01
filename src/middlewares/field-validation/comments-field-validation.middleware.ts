import { body, param } from 'express-validator';

export const validateCommentId = () => {
  return param('id')
    .notEmpty()
    .isString()
    .isLength({ min: 1 })
    .withMessage('Post id should contains more than 1 symbol.')
};
export const validateContent = () => {
  return body('content')
    .notEmpty({ignore_whitespace: true})
    .isLength({ min: 20 })
    .isLength({ max: 300 })
    .withMessage(`Content should contains more than 20 and less than 300 symbols.`);
};
export const validateComments = () => {
  return [
    validateContent(),
  ];
};
