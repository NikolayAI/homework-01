import { body, param } from 'express-validator';
import { emailMatch } from './constants';

export const validateLogin = () => {
  return body('login')
    .isString()
    .notEmpty({ignore_whitespace: true})
    .isLength({ min: 3 })
    .isLength({ max: 10 })
    .withMessage('User login should be valid.')
};

export const validatePassword = () => {
  return body('password')
    .isString()
    .notEmpty({ignore_whitespace: true})
    .isLength({ min: 6 })
    .isLength({ max: 20 })
    .withMessage('User password should be valid.')
};

export const validateEmail = () => {
  return body('email')
    .matches(emailMatch)
    .withMessage('email should be valid.')
};

export const validateUserId = () => {
  return param('id')
    .notEmpty()
    .isString()
    .withMessage('User id should be number that more than 0.');
};
export const validateUsers = () => {
  return [
    validateLogin(),
    validatePassword(),
    validateEmail(),
  ];
};
