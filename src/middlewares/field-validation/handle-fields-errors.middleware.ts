import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator';

export const handleFieldsErrors = () => {
  return (req: Request, res: Response, next: NextFunction) => {
    const errors = validationResult(req);
    const checkedErrors = <string[]>[];
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errorsMessages: errors.array()
          .reduce((acc: any[], error) => {
            if (!checkedErrors.includes(error.param)) {
              checkedErrors.push(error.param);
              acc.push({
                message: error.msg,
                field: error.param,
              });
            }
            return acc;
          }, []),
      });
    }
    next();
  };
};