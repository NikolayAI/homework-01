import { body, param } from 'express-validator';

import { POST_SHORT_DESCRIPTION_MAX_LENGTH } from './constants';
import { ioc } from '../../inversify.config';
import { BloggersRepository } from '../../repositories/bloggers.repository';

const bloggersRepository = ioc.get(BloggersRepository);

export const validatePostId = () => {
  return param('id')
    .notEmpty()
    .isString()
    .isLength({ min: 1 })
    .withMessage('Post id should contains more than 1 symbol.')
};
export const validateTitle = () => {
  return body('title')
    .notEmpty({ignore_whitespace: true})
    .isLength({ min: 2 })
    .withMessage('Title should contains more than 1 symbol.')
    .isLength({ max: 30 })
    .withMessage('Title should contains less than 30 symbols.');
};
export const validateShortDescription = () => {
  return body('shortDescription')
    .notEmpty({ignore_whitespace: true})
    .isLength({ max: POST_SHORT_DESCRIPTION_MAX_LENGTH })
    .withMessage(`Title should contains less than ${POST_SHORT_DESCRIPTION_MAX_LENGTH} symbols.`);
};
export const validateContent = () => {
  return body('content')
    .notEmpty({ignore_whitespace: true})
    .isLength({ min: 2 })
    .withMessage('Content should contains more than 1 symbol.')
    .isLength({ max: 1000 })
    .withMessage('Title should contains less than 20 symbols.');
};
export const validateBlogId = () => {
  return body('blogId')
    .notEmpty()
    .isString()
    .custom(async (id) => {
      const blogger = await bloggersRepository.findOne({ id });
      if (!blogger) throw new Error('blog id is not found');
    })
    .withMessage('Blog id should be number that more than 0.');
};
export const validatePosts = () => {
  return [
    validateTitle(),
    validateShortDescription(),
    validateContent(),
    validateBlogId(),
  ];
};
