import { NextFunction, Request, Response } from 'express';
import { ioc } from '../inversify.config';
import { JwtService } from '../application/jwt.service';
import { UsersService } from '../domain/users.service';

const jwtService = ioc.get(JwtService);
const usersService = ioc.get(UsersService);

export const authTokenUsersMiddleware = () => {
  return async (req: Request, res: Response, next: NextFunction) => {
    if (!req.headers.authorization) return res.send(401);
    const token = req.headers.authorization.split(' ')[1];
    const userId = jwtService.getUserIdByAccessToken(token);
    if (!userId) return res.send(401);
    req.userFromUsers = await usersService.findOneFromUsers({ id: userId });
    next();
  };
};