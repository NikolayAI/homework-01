import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';

import { blogsRouter } from './routes/blogs.router';
import { postsRouter } from './routes/posts.router';
import { videosRouter } from './routes/videos.router';
import { authRouter } from './routes/auth-router';
import { runDb } from './repositories/db';
import { usersRouter } from './routes/users.router';
import { commentsRouter } from './routes/comments.router';
import { testingRouter } from './routes/testing.router';

console.log('HELLO INDEX');

const app = express();
const port = process.env.PORT || 4000;

app.use(cors());
app.use(express.json());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.disable('etag');

app.use('/auth', authRouter);
app.use('/blogs', blogsRouter);
app.use('/posts', postsRouter);
app.use('/videos', videosRouter);
app.use('/users', usersRouter);
app.use('/comments', commentsRouter);
app.use('/testing', testingRouter);

const startApp = async () => {
  try {
    await runDb();
  } catch (e) {
    console.log('db is not connected');
  }
  app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
  });
};

startApp();

