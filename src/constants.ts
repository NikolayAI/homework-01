export const DEFAULT_SORTED_BY = 'createdAt';

export enum SortDirections {
  asc = 'asc',
  desc = 'desc',
}