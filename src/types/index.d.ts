import { IUser, UserAccountDBType } from '../repositories/types';

declare global {
  declare namespace Express {
    export interface Request {
      user: UserAccountDBType | null;
      userFromUsers: Omit<IUser, 'password'> | null;
    }
  }
}