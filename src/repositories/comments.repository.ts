import { mongoose } from './db';
import { IComment, IPagination } from './types';
import { randomUUID } from 'crypto';
import { SortDirection } from 'mongodb';
import { DEFAULT_SORTED_BY, SortDirections } from '../constants';
import { injectable } from 'inversify';
import { SortOrder } from 'mongoose';

const commentSchema = new mongoose.Schema<IComment>({
  id:	{ type: String },
  postId:	{ type: String },
  content: { type: String },
  userId:	{ type: String },
  userLogin: { type: String },
  createdAt: { type: String },
})

const CommentModel = mongoose.model('comments', commentSchema)

@injectable()
export class CommentsRepository {
  async findAll(): Promise<IComment[]> {
    return await CommentModel.find({}, '-_id -__v').exec();
  }

  async findAllWithPagination({
    pageNumber,
    pageSize,
    searchNameTerm,
  }: { pageNumber: number, pageSize: number, searchNameTerm?: string }): Promise<IPagination<IComment>> {
    const filter = {
      ...(searchNameTerm && { title: { $regex: new RegExp(searchNameTerm, 'i') } })
    };
    const start = (pageNumber - 1) * pageSize;
    const items = await CommentModel
      .find(filter, '-_id -__v -postId')
      .limit(pageSize)
      .skip(start)
      .exec();
    const totalCount = await CommentModel.countDocuments(filter).exec();
    const pagesCount = Math.ceil(totalCount / pageSize);

    return {
      pagesCount,
      page: pageNumber,
      pageSize,
      totalCount,
      items,
    };
  }

  async findOne({ id }: { id: string }): Promise<IComment | null> {
    return await CommentModel.findOne({ id }, '-_id -__v -postId').exec();
  }

  async findAllByPostId({
    postId,
    pageNumber,
    pageSize,
    sortBy,
    sortDirection,
  }: {
    postId: string,
    pageNumber: number,
    pageSize: number,
    sortBy: string,
    sortDirection: SortOrder,
  }) {
    const filter = { ...(postId && { postId }) };
    const start = (pageNumber - 1) * pageSize;
    const items = await CommentModel
      .find(filter, '-_id -__v -postId')
      .sort({ [sortBy || DEFAULT_SORTED_BY]: sortDirection || SortDirections.desc })
      .limit(pageSize)
      .skip(start)
      .exec();
    const totalCount = await CommentModel.countDocuments(filter).exec();
    const pagesCount = Math.ceil(totalCount / pageSize);

    return {
      pagesCount,
      page: pageNumber,
      pageSize,
      totalCount,
      items,
    };
  }

  async create({
    postId,
    content,
    userId,
    userLogin,
    createdAt,
  }: Omit<IComment, 'id'>): Promise<Omit<IComment, 'postId'>> {
    const id = randomUUID();
    const newItem = {
      id,
      content,
      userId,
      userLogin,
      createdAt,
    };
    await CommentModel.create({
      ...newItem,
      postId,
    });
    return newItem;
  }

  async update({ id, ...fields }: Pick<IComment, 'id' | 'content'>): Promise<boolean> {
    const updatedItem = await CommentModel
      .updateMany({ id }, { $set: { ...fields } }).exec();
    return updatedItem.matchedCount === 1;
  }

  async remove({ id }: { id: string }): Promise<boolean> {
    const removedItem = await CommentModel.deleteOne({ id }).exec();
    return removedItem.deletedCount === 1;
  }

  async removeAll(): Promise<void> {
    await CommentModel.deleteMany({}).exec();
  }
}
