import { mongoose } from './db';
import {
  EmailConfirmationType,
  IPagination,
  IUser,
  LoginAttemptType, RefreshTokenType,
  SentConfirmationEmailType,
  UserAccountDBType,
  UserAccountType
} from './types';
import { randomUUID } from 'crypto';
import { SortDirection } from 'mongodb';
import { DEFAULT_SORTED_BY, SortDirections } from '../constants';
import { injectable } from 'inversify';
import { SortOrder } from 'mongoose';
import { addSeconds } from 'date-fns';

const userSentConfirmationEmailSchema = new mongoose.Schema<SentConfirmationEmailType>({
  sentDate: { type: Date },
});

const userEmailConfirmationSchema = new mongoose.Schema<EmailConfirmationType>({
  isConfirmed: { type: Boolean },
  confirmationCode: { type: String },
  expirationDate: { type: Date },
  sentEmails: [userSentConfirmationEmailSchema]
});

const useLoginAttemptsSchema = new mongoose.Schema<LoginAttemptType>({
  attemptDate: { type: Date },
  ip: { type: String },
});

const userAccountSchema = new mongoose.Schema<UserAccountType>({
  email: { type: String },
  login: { type: String },
  passwordHash: { type: String },
  createdAt: { type: Date },
});

const refreshTokenSchema = new mongoose.Schema<RefreshTokenType>({
  refreshToken: { type: String },
  expiresIn: { type: Date },
})

const userSchema = new mongoose.Schema<UserAccountDBType>({
  id: { type: String },
  refreshTokens: [refreshTokenSchema],
  blockedRefreshTokens: [String],
  accountData: userAccountSchema,
  loginAttempts: [useLoginAttemptsSchema],
  emailConfirmation: userEmailConfirmationSchema
});

const UsersModel = mongoose.model('users', userSchema);

@injectable()
export class UsersRepository {
  async findAllWithPagination({
    pageNumber,
    pageSize,
    sortBy,
    sortDirection,
    searchLoginTerm,
    searchEmailTerm,
  }: {
    pageNumber: number,
    pageSize: number,
    sortBy: string,
    sortDirection: SortOrder,
    searchLoginTerm?: string
    searchEmailTerm?: string
  }): Promise<IPagination<Omit<IUser, 'password'>>> {
    let filter = {};

    if (searchLoginTerm) {
      filter = { ...filter, 'accountData.login': { $regex: new RegExp(searchLoginTerm, 'i') } };
    }

    if (searchEmailTerm) {
      filter = { ...filter, 'accountData.email': { $regex: new RegExp(searchEmailTerm, 'i') } };
    }

    if (searchLoginTerm && searchEmailTerm) {
      filter = {
        $or: [
          { 'accountData.login': { $regex: new RegExp(searchLoginTerm, 'i') } },
          { 'accountData.email': { $regex: new RegExp(searchEmailTerm, 'i') } },
        ]
      };
    }

    const start = (pageNumber - 1) * pageSize;

    const items = await UsersModel
      .find(filter, '-_id -__v')
      .sort({ [`accountData.${sortBy || DEFAULT_SORTED_BY}`]: sortDirection || SortDirections.desc })
      .limit(pageSize)
      .skip(start)
      .exec()

    const totalCount = await UsersModel.countDocuments(filter).exec();
    const pagesCount = Math.ceil(totalCount / pageSize);

    return {
      pagesCount,
      page: pageNumber,
      pageSize,
      totalCount,
      items: items.map((item) => ({
        id: item.id,
        login: item.accountData?.login,
        email: item.accountData?.email,
        createdAt: item.accountData?.createdAt,
      })),
    };
  }

  async remove({ id }: { id: string }): Promise<boolean> {
    const removedItem = await UsersModel.deleteOne({ id }).exec();
    return removedItem.deletedCount === 1;
  }

  async findOne({ id }: { id: string }): Promise<UserAccountDBType | null> {
    return await UsersModel.findOne({ id }, '-_id -__v').exec();
  }

  async findOneFromUsers({ id }: { id: string }): Promise<Omit<IUser, 'password'> | null> {
    const user = await UsersModel.findOne({ id }, '-_id -__v').exec();

    if (!user) {
      return null;
    }

    return {
      id: user.id,
      login: user.accountData.login,
      email: user.accountData.email,
      createdAt: user.accountData.createdAt,
    };
  }

  async findUsersAccountByLoginOrEmailOrPasswordHash({
    login,
    email,
    passwordHash
  }: { login?: string, email?: string, passwordHash?: string }) {
    const search = [];
    if (login) search.push({ 'accountData.login': login });
    if (email) search.push({ 'accountData.email': email });
    if (passwordHash) search.push({ 'accountData.passwordHash': passwordHash });
    return await UsersModel.findOne({ $or: search }).exec();
  }

  async create(data: Omit<UserAccountDBType, 'id'>): Promise<UserAccountDBType> {
    const newItem: UserAccountDBType = { id: randomUUID(), ...data };
    await UsersModel.create(newItem);

    return newItem;
  }

  async findByConfirmationCode({ code }: { code: string }): Promise<UserAccountDBType | null> {
    return await UsersModel.findOne({ 'emailConfirmation.confirmationCode': code }, '-_id -__v -password').exec();
  }

  async findByRefreshToken({ refreshToken }: { refreshToken: string }): Promise<UserAccountDBType | null> {
    return await UsersModel.findOne({ 'refreshTokens.refreshToken': refreshToken }, '-_id -__v -password').exec();
  }

  async updateConfirmation({ userId }: { userId: string }): Promise<boolean> {
    const result = await UsersModel.updateOne({ id: userId }, { $set: { 'emailConfirmation.isConfirmed': true } }).exec();
    return result.modifiedCount === 1;
  }

  async updateConfirmationCode({ userId, code }: { userId: string, code: string }): Promise<boolean> {
    const result = await UsersModel.updateOne({ id: userId }, { $set: { 'emailConfirmation.confirmationCode': code } }).exec();
    return result.modifiedCount === 1;
  }

  async updateRefreshToken({ userId, refreshTokens }: { userId: string, refreshTokens: RefreshTokenType[] }): Promise<boolean> {
    const result = await UsersModel.updateOne({ id: userId }, { $set: { refreshTokens } }).exec();
    return result.modifiedCount === 1;
  }

  async updateBlockedRefreshToken({ userId, blockedRefreshTokens }: { userId: string, blockedRefreshTokens: string[] }): Promise<boolean> {
    const result = await UsersModel.updateOne({ id: userId }, { $set: { blockedRefreshTokens } }).exec();
    return result.modifiedCount === 1;
  }

  async removeAll(): Promise<void> {
    await UsersModel.deleteMany({}).exec();
  }
}
