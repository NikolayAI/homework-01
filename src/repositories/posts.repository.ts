import { mongoose } from './db';
import { IPagination, IPost } from './types';
import { randomUUID } from 'crypto';
import { SortDirection } from 'mongodb';
import { DEFAULT_SORTED_BY, SortDirections } from '../constants';
import { injectable } from 'inversify';
import { SortOrder } from 'mongoose';

const postSchema = new mongoose.Schema<IPost>({
  id: { type: String },
  title: { type: String },
  shortDescription: { type: String },
  content: { type: String },
  blogId: { type: String },
  blogName: { type: String },
  createdAt: { type: Date },
})

const PostsModel = mongoose.model('posts', postSchema);

@injectable()
export class PostsRepository {
  async findAll(): Promise<IPost[]> {
    return await PostsModel.find({}, '-_id -__v').exec();
  }

  async findAllWithPagination({
    pageNumber,
    pageSize,
    sortBy,
    sortDirection,
    searchNameTerm,
  }: { pageNumber: number, pageSize: number, sortBy: string, sortDirection: SortOrder, searchNameTerm?: string }): Promise<IPagination<IPost>> {
    const filter = {
      ...(searchNameTerm && { title: { $regex: new RegExp(searchNameTerm, 'i') } })
    };
    const start = (pageNumber - 1) * pageSize;
    const items = await PostsModel
      .find(filter, '-_id -__v')
      .sort({ [sortBy || DEFAULT_SORTED_BY]: sortDirection || SortDirections.desc })
      .limit(pageSize)
      .skip(start)
      .exec();
    const totalCount = await PostsModel.countDocuments(filter).exec();
    const pagesCount = Math.ceil(totalCount / pageSize);

    return {
      pagesCount,
      page: pageNumber,
      pageSize,
      totalCount,
      items,
    };
  }

  async findOne({ id }: { id: string }): Promise<IPost | null> {
    return await PostsModel.findOne({ id }, '-_id -__v').exec();
  }

  async findAllByBlogId({
    blogId,
    pageNumber,
    pageSize,
    sortBy,
    sortDirection,
  }: { blogId: string, pageNumber: number, pageSize: number, sortBy: string, sortDirection: SortOrder }) {
    const filter = { ...(blogId && { blogId }) };
    const start = (pageNumber - 1) * pageSize;
    const items = await PostsModel
      .find(filter, '-_id -__v')
      .sort({ [sortBy || DEFAULT_SORTED_BY]: sortDirection || SortDirections.desc })
      .limit(pageSize)
      .skip(start)
      .exec();
    const totalCount = await PostsModel.countDocuments(filter).exec();
    const pagesCount = Math.ceil(totalCount / pageSize);

    return {
      pagesCount,
      page: pageNumber,
      pageSize,
      totalCount,
      items,
    };
  }

  async create({
    title,
    content,
    shortDescription,
    blogId,
    blogName,
  }: Omit<IPost, 'id' | 'createdAt'>): Promise<IPost> {
    const newItem: IPost = {
      id: randomUUID(),
      blogId,
      title,
      content,
      shortDescription,
      blogName: blogName ?? 'new blog',
      createdAt: new Date(),
    };
    await PostsModel.create(newItem);
    return newItem;
  }

  async update({
    id,
    title,
    content,
    shortDescription,
    blogId,
  }: IPost): Promise<boolean> {
    const updatedItem = await PostsModel
      .updateOne({ id }, {
        $set: {
          title,
          content,
          shortDescription,
          blogId
        }
      }).exec();
    return updatedItem.matchedCount === 1;
  }

  async remove({ id }: { id: string }): Promise<boolean> {
    const removedItem = await PostsModel.deleteOne({ id }).exec();
    return removedItem.deletedCount === 1;
  }

  async removeAll(): Promise<void> {
    await PostsModel.deleteMany({}).exec();
  }
}