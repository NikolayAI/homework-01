import { createTransport } from 'nodemailer';
import dotenv from 'dotenv';
import { injectable } from 'inversify';

dotenv.config();

export interface IEmailRepositorySendParams {
  to: string;
  subject: string;
  html: string;
}

@injectable()
export class EmailRepository {
  async send({ subject, html, to }: IEmailRepositorySendParams) {
    const transporter = createTransport({
      service: 'Yandex',
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASS,
      }
    });

    return await transporter.sendMail({
      from: 'Nikolay <alexeevn042@yandex.ru>',
      to,
      subject,
      html,
    });
  }
}