import { WithId } from 'mongodb';

export interface IUser {
  id: string;
  login: string | null;
  password: string;
  email: string;
  createdAt: Date;
}
export interface IUserDto {
  login: string;
  password: string;
  email: string;
  createdAt: Date;
}

export interface IBlog {
  id: string;
  name: string | null;
  websiteUrl: string | null;
  createdAt: Date;
  description: string;
}
export interface IPagination<T> {
  page: number | undefined;
  pagesCount: number;
  pageSize: number | undefined;
  totalCount: number;
  items: T[]
}
export interface IComment {
  id:	string | null
  postId:	string | null
  content: string | null
  userId:	string | null
  userLogin: string | null
  createdAt: string
}

export interface IPost {
  id: string;
  title: string | null;
  shortDescription: string | null;
  content: string | null;
  blogId: string;
  blogName: string | null;
  createdAt: Date;
}

export interface IPostDto {
  id: string;
  title: string | null;
  shortDescription: string | null;
  content: string | null;
  blogId: string;
  blogName: string | null;
  createdAt: Date;
}

export interface IVideo {
  id: string;
  title: string | null;
  author: string | null;
}

export type EmailConfirmationType = {
  isConfirmed: boolean
  confirmationCode: string
  expirationDate: Date
  sentEmails: SentConfirmationEmailType[]
}

export type UserAccountType = {
  email: string
  login: string
  passwordHash: string
  createdAt: Date
}
export type SentConfirmationEmailType = {
  sentDate: Date
}

export type LoginAttemptType = {
  attemptDate: Date
  ip: string
}

export type RefreshTokenType = {
  refreshToken: string;
  expiresIn: Date;
}

export type UserAccountDBType = WithId<{
  id: string;
  refreshTokens: RefreshTokenType[];
  blockedRefreshTokens: string[];
  accountData: UserAccountType,
  loginAttempts: LoginAttemptType[],
  emailConfirmation: EmailConfirmationType
}>