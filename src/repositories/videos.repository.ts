import { mongoose } from './db';
import { IVideo } from './types';
import { randomUUID } from 'crypto';
import { injectable } from 'inversify';

const videoSchema = new mongoose.Schema<IVideo>({
  id: { type: String },
  title: { type: String },
  author: { type: String },
})

const VideosModel = mongoose.model('videos', videoSchema)

@injectable()
export class VideosRepository {
  async findAll(): Promise<IVideo[]> {
    return await VideosModel.find({}, '-_id -__v').exec();
  }

  async findOne({ id }: { id: string }): Promise<IVideo | null> {
    return await VideosModel.findOne({ id }, '-_id -__v').exec();
  }

  async create({
    author = '',
    title
  }: Omit<IVideo, 'id'>): Promise<IVideo> {
    const newItem: IVideo = { id: randomUUID(), title, author };
    await VideosModel.create(newItem);
    return newItem;
  }

  async update({
    id,
    ...fields
  }: IVideo): Promise<boolean> {
    const updatedItem = await VideosModel
      .updateMany({ id }, { $set: { ...fields } }).exec();
    return updatedItem.matchedCount === 1;
  }

  async remove({ id }: { id: string }): Promise<boolean> {
    const removedItem = await VideosModel.deleteOne({ id }).exec();
    return removedItem.deletedCount === 1;
  }

  async removeAll(): Promise<void> {
    await VideosModel.deleteMany({}).exec();
  }
}
