import { mongoose } from './db';
import { IBlog, IPagination } from './types';
import { randomUUID } from 'crypto';
import { SortDirection } from 'mongodb';
import { DEFAULT_SORTED_BY, SortDirections } from '../constants';
import { injectable } from 'inversify';
import { SortOrder } from 'mongoose';

const blogSchema = new mongoose.Schema<IBlog>({
  id: { type: String },
  name: { type: String },
  websiteUrl: { type: String },
  description: { type: String },
  createdAt: { type: Date, required: true, default: new Date },
});

const BlogsModel = mongoose.model('blogs', blogSchema);

@injectable()
export class BloggersRepository {
  async findAll(): Promise<IBlog[]> {
    return await BlogsModel.find({}, '-_id -__v').exec();
  }

  async findAllWithPagination({
    pageNumber,
    pageSize,
    searchNameTerm,
    sortBy,
    sortDirection,
  }: { pageNumber: number, pageSize: number, sortBy: string, sortDirection: SortOrder, searchNameTerm?: string }): Promise<IPagination<IBlog>> {
    const filter = {
      ...(searchNameTerm && { name: { $regex: new RegExp(searchNameTerm, 'i') } })
    };
    const start = (pageNumber - 1) * pageSize;
    const items = await BlogsModel
      // .find(filter, '-_id -__v')
      .find()
      .sort({ [sortBy || DEFAULT_SORTED_BY]: sortDirection || SortDirections.desc })
      .limit(pageSize)
      .skip(start)
      .exec();
    const totalCount = await BlogsModel.countDocuments(filter).exec();
    const pagesCount = Math.ceil(totalCount / pageSize);

    return {
      pagesCount,
      page: pageNumber,
      pageSize,
      totalCount,
      items,
    };
  }

  async findOne({ id }: { id: string }): Promise<IBlog | null> {
    return await BlogsModel.findOne({ id }, '-_id -__v').exec();
  }

  async create({
    name,
    websiteUrl,
    description,
  }: Omit<IBlog, 'id' | 'createdAt'>): Promise<IBlog> {
    const newItem: IBlog = {
      id: randomUUID(),
      createdAt: new Date(),
      name,
      websiteUrl,
      description,
    };
    await BlogsModel.create(newItem);
    return newItem;
  }

  async update({ id, ...fields }: IBlog): Promise<boolean> {
    const updatedItem = await BlogsModel
      .updateMany({ id }, { $set: { ...fields } }).exec();
    return updatedItem.matchedCount === 1;
  }

  async remove({ id }: { id: string }): Promise<boolean> {
    const removedItem = await BlogsModel.deleteOne({ id }).exec();
    return removedItem.deletedCount === 1;
  }

  async removeAll(): Promise<void> {
    await BlogsModel.deleteMany({}).exec();
  }
}
