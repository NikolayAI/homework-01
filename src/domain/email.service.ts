import { EmailRepository } from '../repositories/email.repository';
import { inject, injectable } from 'inversify';

@injectable()
export class EmailService {
  constructor(
    @inject(EmailRepository) protected emailRepository: EmailRepository,
  ) {
  }

  async registration({ to, code }: { to: string, code: string }) {
    return await this.emailRepository.send({
      subject: 'Регистрация в приложении.',
      html: '<a href="https://somesite.com/confirm-email?code=' + code + '"/>',
      to,
    });
  }
}