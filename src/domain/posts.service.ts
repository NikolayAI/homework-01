import { IPagination, IPost, IPostDto, } from '../repositories/types';
import { SortDirection, WithId } from 'mongodb';
import { PostsRepository } from '../repositories/posts.repository';
import { BloggersRepository } from '../repositories/bloggers.repository';
import { inject, injectable } from 'inversify';
import { SortOrder } from 'mongoose';

@injectable()
export class PostsService {
  constructor(
    @inject(PostsRepository) protected postsRepository: PostsRepository,
    @inject(BloggersRepository) protected bloggersRepository: BloggersRepository,
  ) {
  }

  async findAll(): Promise<IPostDto[]> {
    const posts = await this.postsRepository.findAll();
    const bloggers = await this.bloggersRepository.findAll();
    return posts.map((item) => ({
      ...item,
      blogName: bloggers.find((blogger) => blogger.id === item.blogId)?.name ?? null,
    }));
  }

  async findAllWithPagination({
    pageNumber,
    pageSize,
    searchNameTerm,
    sortBy,
    sortDirection,
  }: { pageNumber: number, pageSize: number, sortBy: string, sortDirection: SortOrder, searchNameTerm?: string }): Promise<IPagination<IPost>> {
    return await this.postsRepository.findAllWithPagination({
      pageNumber,
      pageSize,
      sortBy,
      sortDirection,
      searchNameTerm,
    });
  }

  async findOne({ id }: { id: string }): Promise<IPostDto | null> {
    const item = await this.postsRepository.findOne({ id });
    if (item) {
      const blogger = await this.bloggersRepository.findOne({ id: item.blogId });
      return {
        id: item.id,
        title: item.title,
        shortDescription: item.shortDescription,
        content: item.content,
        blogId: item.blogId,
        createdAt: item.createdAt,
        blogName: blogger?.name ?? null,
      };
    }
    return null;
  }

  async findAllByBlogId({
    blogId,
    pageNumber,
    pageSize,
    sortBy,
    sortDirection,
  }: { blogId: string, pageNumber: number, pageSize: number, sortBy: string, sortDirection: SortOrder }): Promise<IPagination<WithId<IPost>> | null> {
    const blogger = await this.bloggersRepository.findOne({ id: blogId });
    if (!blogger) return null;
    return await this.postsRepository.findAllByBlogId({
      blogId,
      pageNumber,
      pageSize,
      sortBy,
      sortDirection,
    });
  }

  async create({
    title,
    content,
    shortDescription,
    blogId,
    blogName,
  }: Omit<IPost, 'id' | 'createdAt'>): Promise<IPostDto | null> {
    const blogger = await this.bloggersRepository.findOne({ id: blogId });
    if (!blogger) return null;
    const name = blogger?.name ?? blogName ?? 'new blog';
    const newItem = await this.postsRepository.create({
      title,
      content,
      shortDescription,
      blogId,
      blogName: name,
    });
    return {
      id: newItem.id,
      title: newItem.title,
      content: newItem.content,
      shortDescription: newItem.shortDescription,
      blogId: newItem.blogId,
      createdAt: newItem.createdAt,
      blogName: name,
    };
  }

  async update({
    id,
    ...fields
  }: IPost): Promise<boolean | null> {
    const blogger = await this.bloggersRepository.findOne({ id: fields.blogId });
    if (!blogger) return null;
    return await this.postsRepository.update({
      id,
      ...fields,
    });
  }

  async remove({ id }: { id: string }): Promise<boolean> {
    return await this.postsRepository.remove({ id });
  }

  async removeAll(): Promise<void> {
    await this.postsRepository.removeAll();
  }
}