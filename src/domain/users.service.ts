import { IPagination, IUser, IUserDto, RefreshTokenType, UserAccountDBType } from '../repositories/types';
import { ObjectId, SortDirection } from 'mongodb';
import { randomUUID } from 'crypto';
import addMinutes from 'date-fns/addMinutes';
import { UsersRepository } from '../repositories/users.repository';
import { AuthService } from './auth.service';
import { inject, injectable } from 'inversify';
import { SortOrder } from 'mongoose';

@injectable()
export class UsersService {
  constructor(
    @inject(UsersRepository) protected usersRepository: UsersRepository,
    @inject(AuthService) protected authService: AuthService,
  ) {
  }

  async findAllWithPagination({
    pageNumber,
    pageSize,
    sortBy,
    sortDirection,
    searchLoginTerm,
    searchEmailTerm,
  }: {
    pageNumber: number,
    pageSize: number,
    sortBy: string,
    sortDirection: SortOrder,
    searchLoginTerm?: string
    searchEmailTerm?: string
  }): Promise<IPagination<Omit<IUser, 'password'>>> {
    return await this.usersRepository.findAllWithPagination({
      pageNumber,
      pageSize,
      sortBy,
      sortDirection,
      searchLoginTerm,
      searchEmailTerm,
    });
  }

  async create({
    login,
    password,
    email,
  }: Omit<IUserDto, 'createdAt'>): Promise<Omit<IUser, 'password'>> {
    const passwordHash = await this.authService._generateHash(password);
    const newUser: UserAccountDBType = {
      _id: new ObjectId(),
      id: randomUUID(),
      refreshTokens: [],
      blockedRefreshTokens: [],
      accountData: {
        login,
        email,
        passwordHash,
        createdAt: new Date()
      },
      loginAttempts: [],
      emailConfirmation: {
        sentEmails: [],
        confirmationCode: randomUUID(),
        expirationDate: addMinutes(new Date(), 30),
        isConfirmed: true
      }
    };
    const user = await this.usersRepository.create(newUser);
    return {
      id: user.id,
      login: user.accountData.login,
      email: user.accountData.email,
      createdAt: user.accountData.createdAt,
    };
  }

  async remove({ id }: { id: string }): Promise<boolean> {
    return await this.usersRepository.remove({ id });
  }

  async findOne({ id }: { id: string }): Promise<UserAccountDBType | null> {
    return await this.usersRepository.findOne({ id });
  }

  async findByRefreshToken({ refreshToken }: { refreshToken: string }): Promise<UserAccountDBType | null> {
    return await this.usersRepository.findByRefreshToken({ refreshToken });
  }

  async findOneFromUsers({ id }: { id: string }): Promise<Omit<IUser, 'password'> | null> {
    return await this.usersRepository.findOneFromUsers({ id });
  }

  async addRefreshToken(userId: string, refreshToken: RefreshTokenType): Promise<boolean> {
    const user = await this.usersRepository.findOne({ id: userId }) ?? { refreshTokens: [] };
    const refreshTokens = [...user.refreshTokens, refreshToken];
    return await this.usersRepository.updateRefreshToken({ userId, refreshTokens });
  }

  async removeRefreshToken(userId: string, oldRefreshToken: string): Promise<boolean> {
    const user = await this.usersRepository.findOne({ id: userId }) ?? { refreshTokens: [] };
    const refreshTokens = user.refreshTokens.filter((rt) => rt.refreshToken !== oldRefreshToken);
    return await this.usersRepository.updateRefreshToken({ userId, refreshTokens });
  }

  async addBlockedRefreshToken(userId: string, blockedRefreshToken: string): Promise<boolean> {
    const user = await this.usersRepository.findOne({ id: userId }) ?? { blockedRefreshTokens: [] };
    const refreshTokens = [...user.blockedRefreshTokens, blockedRefreshToken];
    return await this.usersRepository.updateBlockedRefreshToken({ userId, blockedRefreshTokens: refreshTokens });
  }

  async removeAll(): Promise<void> {
    await this.usersRepository.removeAll();
  }
}