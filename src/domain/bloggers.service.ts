import { IBlog, IPagination } from '../repositories/types';
import { SortDirection } from 'mongodb';
import { BloggersRepository } from '../repositories/bloggers.repository';
import { inject, injectable } from 'inversify';
import { SortOrder } from 'mongoose';

@injectable()
export class BloggersService {
  constructor(
    @inject(BloggersRepository) protected bloggersRepository: BloggersRepository,
  ) {
  }

  async findAll(): Promise<IBlog[]> {
    return await this.bloggersRepository.findAll();
  }

  async findAllWithPagination({
    pageNumber,
    pageSize,
    searchNameTerm,
    sortBy,
    sortDirection,
  }: { pageNumber: number, pageSize: number, sortBy: string, sortDirection: SortOrder, searchNameTerm?: string }): Promise<IPagination<IBlog>> {
    return await this.bloggersRepository.findAllWithPagination({
      pageNumber,
      pageSize,
      sortBy,
      sortDirection,
      searchNameTerm,
    });
  }

  async findOne({ id }: { id: string }): Promise<IBlog | null> {
    return await this.bloggersRepository.findOne({ id });
  }

  async create({
    name,
    websiteUrl,
    description,
  }: Omit<IBlog, 'id' | 'createdAt'>): Promise<IBlog> {
    return await this.bloggersRepository.create({ name, websiteUrl, description });
  }

  async update({
    id,
    ...fields
  }: IBlog): Promise<boolean> {
    return await this.bloggersRepository.update({ id, ...fields });
  }

  async remove({ id }: { id: string }): Promise<boolean> {
    return await this.bloggersRepository.remove({ id });
  }

  async removeAll(): Promise<void> {
    await this.bloggersRepository.removeAll();
  }
}
