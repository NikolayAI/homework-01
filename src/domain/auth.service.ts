import { UserAccountDBType } from '../repositories/types';
import { ObjectId } from 'mongodb';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { settings } from '../settings';
import { randomUUID } from 'crypto';
import addMinutes from 'date-fns/addMinutes';
import { UsersRepository } from '../repositories/users.repository';
import { EmailService } from './email.service';
import { inject, injectable } from 'inversify';

@injectable()
export class AuthService {
  constructor(
    @inject(UsersRepository) protected usersRepository: UsersRepository,
    @inject(EmailService) protected emailService: EmailService,
    ) {
  }

  async createUser(login: string, email: string, password: string): Promise<UserAccountDBType | null> {
    const passwordHash = await this._generateHash(password);
    const newUser: UserAccountDBType = {
      _id: new ObjectId(),
      id: randomUUID(),
      refreshTokens: [],
      blockedRefreshTokens: [],
      accountData: {
        login,
        email,
        passwordHash,
        createdAt: new Date()
      },
      loginAttempts: [],
      emailConfirmation: {
        sentEmails: [],
        confirmationCode: randomUUID(),
        expirationDate: addMinutes(new Date(), 30),
        isConfirmed: false
      }
    };

    try {
      await this.emailService.registration({
        to: email,
        code: newUser.emailConfirmation.confirmationCode,
      });
    } catch (e) {
      console.error(e);
      return null;
    }

    return await this.usersRepository.create(newUser);
  }

  async checkCredentials(login: string, password: string) {
    const user = await this.usersRepository.findUsersAccountByLoginOrEmailOrPasswordHash({ login });
    if (!user) return null;

    const isHashesEquals = await this._isHashesEquals(password, user.accountData.passwordHash);

    if (isHashesEquals) {
      return user;
    } else {
      return null;
    }
  }

  async _generateHash(password: string) {
    const hash = await bcrypt.hash(password, 10);
    return hash;
  }

  async _isHashesEquals(hash1: string, hash2: string) {
    return await bcrypt.compare(hash1, hash2);
  }

  async checkAndFindUserByToken(token: string) {
    try {
      const result: any = jwt.verify(token, settings.JWT_SECRET);
      const user = await this.usersRepository.findOne({ id: result.userId });
      return user;
    } catch (error) {
      return null;
    }
  }

  async confirmEmail(code: string): Promise<boolean> {
    const user = await this.usersRepository.findByConfirmationCode({ code });

    if (!user) return false;

    if (user.emailConfirmation.isConfirmed) return false;
    if (user.emailConfirmation.confirmationCode !== code) return false;
    if (user.emailConfirmation.expirationDate < new Date()) return false;

    const isConf = await this.usersRepository.updateConfirmation({ userId: user.id });

    return isConf;
  }

  async resendEmail(email: string): Promise<boolean> {
    const user = await this.usersRepository.findUsersAccountByLoginOrEmailOrPasswordHash({ email });

    if (!user) return false;
    if (user.emailConfirmation.isConfirmed) return false;

    const newCode = randomUUID();

    await this.usersRepository.updateConfirmationCode({ userId: user.id, code: newCode });

    try {
      await this.emailService.registration({
        to: email,
        code: newCode,
      });
    } catch (e) {
      console.error(e);
      return false;
    }

    return true;
  }
}
