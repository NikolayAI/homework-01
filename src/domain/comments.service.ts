import { CommentsRepository } from '../repositories/comments.repository';
import { IComment, IPagination, } from '../repositories/types';
import { SortDirection, WithId } from 'mongodb';
import { inject, injectable } from 'inversify';
import { PostsService } from './posts.service';
import { SortOrder } from 'mongoose';

@injectable()
export class CommentsService {
  constructor(
    @inject(CommentsRepository) protected commentsRepository: CommentsRepository,
    @inject(PostsService) protected postsService: PostsService,
  ) {
  }

  async findAll(): Promise<IComment[]> {
    return await this.commentsRepository.findAll();
  }

  async findAllWithPagination({
    pageNumber,
    pageSize,
    searchNameTerm,
  }: { pageNumber: number, pageSize: number, searchNameTerm?: string }): Promise<IPagination<IComment>> {
    return await this.commentsRepository.findAllWithPagination({
      pageNumber,
      pageSize,
      searchNameTerm,
    });
  }


  async findAllByPostId({
    postId,
    pageNumber,
    pageSize,
    sortBy,
    sortDirection,
  }: {
    postId: string,
    pageNumber: number,
    pageSize: number,
    sortBy: string,
    sortDirection: SortOrder,
  }): Promise<IPagination<WithId<IComment>> | null> {
    const post = await this.postsService.findOne({ id: postId });
    if (!post) return null;
    return await this.commentsRepository.findAllByPostId({
      postId,
      pageNumber,
      pageSize,
      sortBy,
      sortDirection,
    });
  }

  async findOne({ id }: { id: string }): Promise<IComment | null> {
    const item = await this.commentsRepository.findOne({ id });
    if (item) {
      return item;
    }
    return null;
  }

  async create({
    postId,
    content,
    userId,
    userLogin,
    createdAt,
  }: Omit<IComment, 'id'>): Promise<Omit<IComment, 'postId'> | null> {
    return await this.commentsRepository.create({
      postId,
      content,
      userId,
      userLogin,
      createdAt,
    });
  }

  async update({
    id,
    ...fields
  }: IComment): Promise<boolean | null> {
    return await this.commentsRepository.update({
      id,
      ...fields,
    });
  }

  async remove({ id }: { id: string }): Promise<boolean> {
    return await this.commentsRepository.remove({ id });
  }

  async removeAll(): Promise<void> {
    await this.commentsRepository.removeAll();
  }
}