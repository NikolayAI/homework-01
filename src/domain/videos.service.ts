import { IVideo } from '../repositories/types';
import { VideosRepository } from '../repositories/videos.repository';
import { inject, injectable } from 'inversify';

@injectable()
export class VideosService {
  constructor(@inject(VideosRepository) protected videosRepository: VideosRepository) {
  }

  async findAll(): Promise<IVideo[]> {
    return await this.videosRepository.findAll();
  }

  async findOne({ id }: { id: string }): Promise<IVideo | null> {
    return await this.videosRepository.findOne({ id });
  }

  async create({
    author,
    title
  }: Omit<IVideo, 'id'>): Promise<IVideo> {
    return await this.videosRepository.create({ author, title });
  }

  async update({
    id,
    ...fields
  }: IVideo): Promise<boolean> {
    return await this.videosRepository.update({ id, ...fields });
  }

  async remove({ id }: { id: string }): Promise<boolean> {
    return await this.videosRepository.remove({ id });
  }

  async removeAll(): Promise<void> {
    await this.videosRepository.removeAll();
  }
}