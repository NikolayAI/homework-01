import "reflect-metadata";
import { Container } from "inversify";

import { JwtService } from './application/jwt.service';
import { AuthController } from './controllers/auth.controller';
import { AuthService } from './domain/auth.service';
import { BloggersRepository } from './repositories/bloggers.repository';
import { BloggersService } from './domain/bloggers.service';
import { BloggersController } from './controllers/bloggers.controller';
import { CommentsRepository } from './repositories/comments.repository';
import { CommentsService } from './domain/comments.service';
import { CommentsController } from './controllers/comments.controller';
import { EmailRepository } from './repositories/email.repository';
import { EmailService } from './domain/email.service';
import { PostsRepository } from './repositories/posts.repository';
import { PostsService } from './domain/posts.service';
import { PostsController } from './controllers/posts.controller';
import { UsersRepository } from './repositories/users.repository';
import { UsersService } from './domain/users.service';
import { UsersController } from './controllers/users.controller';
import { VideosRepository } from './repositories/videos.repository';
import { VideosService } from './domain/videos.service';
import { VideosController } from './controllers/videos.controller';
import { TestingController } from './controllers/testing.controller';

export const ioc = new Container();

ioc.bind(BloggersRepository).toSelf();
ioc.bind(CommentsRepository).toSelf();
ioc.bind(EmailRepository).toSelf();
ioc.bind(PostsRepository).toSelf();
ioc.bind(UsersRepository).toSelf();
ioc.bind(VideosRepository).toSelf();

ioc.bind(BloggersService).toSelf();
ioc.bind(CommentsService).toSelf();
ioc.bind(EmailService).toSelf();
ioc.bind(AuthService).toSelf();
ioc.bind(JwtService).toSelf();
ioc.bind(PostsService).toSelf();
ioc.bind(UsersService).toSelf();
ioc.bind(VideosService).toSelf();

ioc.bind(AuthController).toSelf();
ioc.bind(BloggersController).toSelf();
ioc.bind(CommentsController).toSelf();
ioc.bind(PostsController).toSelf();
ioc.bind(TestingController).toSelf();
ioc.bind(UsersController).toSelf();
ioc.bind(VideosController).toSelf();
