import { Request, Response } from 'express';
import { CommentsService } from '../domain/comments.service';
import { inject, injectable } from 'inversify';

@injectable()
export class CommentsController {
  constructor(
    @inject(CommentsService) protected commentsService: CommentsService,
  ) {
  }

  async findOne(req: Request, res: Response) {
    const item = await this.commentsService.findOne({ id: req.params.id });
    if (!item) return res.send(404);
    return res.send(item);
  }

  async update(req: Request, res: Response) {
    const item = await this.commentsService.findOne({ id: req.params.id });

    if (!item) return res.send(404);
    if (item?.userId !== req.user?.id) return res.send(403);

    const { content } = req.body || {};

    const fields = {
      ...(content && { content }),
    };

    const updatedItem = await this.commentsService.update({
      id: req.params.id,
      ...fields,
    });
    if (updatedItem === null) return res.send(400);
    if (!updatedItem) return res.send(404);
    res.send(204);
  }

  async remove(req: Request, res: Response) {
    const item = await this.commentsService.findOne({ id: req.params.id });

    if (!item) return res.send(404);
    if (item?.userId !== req.user?.id) return res.send(403);

    const isItemDeleted = await this.commentsService.remove({ id: req.params.id });
    if (!isItemDeleted) return res.send(404);
    res.send(204);
  }
}