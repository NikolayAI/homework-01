import { Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import { UsersService } from '../domain/users.service';
import { BloggersService } from '../domain/bloggers.service';
import { PostsService } from '../domain/posts.service';
import { VideosService } from '../domain/videos.service';
import { CommentsService } from '../domain/comments.service';

@injectable()
export class TestingController {
  constructor(
    @inject(UsersService) protected usersService: UsersService,
    @inject(BloggersService) protected bloggersService: BloggersService,
    @inject(PostsService) protected postsService: PostsService,
    @inject(VideosService) protected videosService: VideosService,
    @inject(CommentsService) protected commentsService: CommentsService,
  ) {
  }

  async allData(req: Request, res: Response) {
    try {
      await this.usersService.removeAll();
      await this.bloggersService.removeAll();
      await this.postsService.removeAll();
      await this.videosService.removeAll();
      await this.commentsService.removeAll();

      res.sendStatus(204);
    } catch (e) {
      console.error(e);
    }
  }
}