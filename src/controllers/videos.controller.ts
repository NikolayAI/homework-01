import { Request, Response } from 'express';
import { VideosService } from '../domain/videos.service';
import { inject, injectable } from 'inversify';

@injectable()
export class VideosController {
  constructor(@inject(VideosService) protected videosService: VideosService) {
  }

  async findAll(req: Request, res: Response) {
    const items = await this.videosService.findAll();
    res.status(200).send(items);
  }

  async create(req: Request, res: Response) {
    const title = req.body.title;
    const author = req.body.author;
    const newItem = await this.videosService.create({ title, author });
    res.status(201).send(newItem);
  }

  async findOne(req: Request, res: Response) {
    const item = await this.videosService.findOne({ id: req.params.id });
    if (!item) return res.send(404);
    res.send(item);
  }

  async update(req: Request, res: Response) {
    const fields = {
      ...(req.body.title && { title: req.body.title }),
      ...(req.body.author && { author: req.body.author }),
    };
    const isUpdated = await this.videosService.update({
      id: req.params.id,
      ...fields
    });
    if (!isUpdated) return res.send(404);
    res.send(204);
  }

  async remove(req: Request, res: Response) {
    const isRemoved = await this.videosService.remove({ id: req.params.id });
    if (!isRemoved) return res.send(404);
    res.send(204);
  }
}