import { Request, Response } from 'express';
import { UsersService } from '../domain/users.service';
import { inject, injectable } from 'inversify';

@injectable()
export class UsersController {
  constructor(
    @inject(UsersService) protected usersService: UsersService,
  ) {
  }

  async findAll(req: Request, res: Response) {
    const params = {
      pageNumber: Number(req.query.pageNumber) || 1,
      pageSize: Number(req.query.pageSize) || 10,
      sortBy: req.query.sortBy,
      sortDirection: req.query.sortDirection,
      ...(req.query.searchLoginTerm && { searchLoginTerm: String(req.query.searchLoginTerm) || undefined }),
      ...(req.query.searchEmailTerm && { searchEmailTerm: String(req.query.searchEmailTerm) || undefined }),
    };
    // @ts-ignore
    const items = await this.usersService.findAllWithPagination(params);
    res.status(200).send(items);
  }

  async create(req: Request, res: Response) {
    const login = req.body.login;
    const password = req.body.password;
    const email = req.body.email;
    const newItem = await this.usersService.create({ login, password, email });
    res.status(201).send(newItem);
  }

  async findOne(req: Request, res: Response) {
    const item = await this.usersService.findOne({ id: req.params.id });
    if (!item) return res.send(404);
    return res.send(item);
  }

  async remove(req: Request, res: Response) {
    const isRemoved = await this.usersService.remove({ id: req.params.id });
    if (!isRemoved) return res.send(404);
    res.send(204);
  }
}