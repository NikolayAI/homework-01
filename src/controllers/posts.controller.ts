import { Request, Response } from 'express';
import { DEFAULT_SORTED_BY, SortDirections } from '../constants';
import { PostsService } from '../domain/posts.service';
import { CommentsService } from '../domain/comments.service';
import { inject, injectable } from 'inversify';

@injectable()
export class PostsController {
  constructor(
    @inject(PostsService) protected postsService: PostsService,
    @inject(CommentsService) protected commentsService: CommentsService,
  ) {
  }

  async findAll(req: Request, res: Response) {
    const params = {
      pageNumber: Number(req.query.pageNumber) || 1,
      pageSize: Number(req.query.pageSize) || 10,
      sortBy: req.query.sortBy || DEFAULT_SORTED_BY,
      sortDirection: req.query.sortDirection || SortDirections.desc,
      ...(req.query.SearchNameTerm && { searchNameTerm: String(req.query.SearchNameTerm) || undefined }),
    };
    // @ts-ignore
    const items = await this.postsService.findAllWithPagination(params);
    res.send(items);
  }

  async create(req: Request, res: Response) {
    const newItem = await this.postsService.create({
      title: req.body?.title,
      content: req.body?.content,
      shortDescription: req.body?.shortDescription,
      blogName: req.body?.blogName,
      blogId: req.body?.blogId
    });
    if (!newItem) return res.send(400);
    res.status(201).send(newItem);
  }

  async findOne(req: Request, res: Response) {
    const item = await this.postsService.findOne({ id: req.params.id });
    if (!item) return res.send(404);
    return res.send(item);
  }

  async update(req: Request, res: Response) {
    const {
      title,
      shortDescription,
      content,
      blogId,
    } = req.body || {};

    const fields = {
      ...(blogId && { blogId }),
      ...(title && { title }),
      ...(content && { content }),
      ...(shortDescription && { shortDescription }),
    };

    const updatedItem = await this.postsService.update({
      id: req.params.id,
      ...fields,
    });
    if (updatedItem === null) return res.send(400);
    if (!updatedItem) return res.send(404);
    res.send(204);
  }

  async remove(req: Request, res: Response) {
    const isItemDeleted = await this.postsService.remove({ id: req.params.id });
    if (!isItemDeleted) return res.send(404);
    res.send(204);
  }

  async findAllComments(req: Request, res: Response) {
    const item = await this.postsService.findOne({ id: req.params.id });
    if (!item) return res.send(404);
    const params = {
      postId: req.params.id,
      pageNumber: Number(req.query.pageNumber) || 1,
      pageSize: Number(req.query.pageSize) || 10,
      sortBy: req.query.sortBy,
      sortDirection: req.query.sortDirection,
    };
    // @ts-ignore
    const items = await this.commentsService.findAllByPostId(params);
    res.send(items);
  }

  async createComment(req: Request, res: Response) {
    const item = await this.postsService.findOne({ id: req.params.id });
    if (!item) return res.send(404);

    const newItem = await this.commentsService.create({
      postId: item.id,
      content: req.body.content,
      userLogin: req.user!.accountData.login,
      userId: req.user!.id,
      createdAt: new Date().toISOString(),
    });

    if (!newItem) return res.send(400);
    res.status(201).send(newItem);
  }
}