import { Request, Response } from 'express';
import { BloggersService } from '../domain/bloggers.service';
import { PostsService } from '../domain/posts.service';
import { inject, injectable } from 'inversify';

@injectable()
export class BloggersController {
  constructor(
    @inject(BloggersService) protected bloggersService: BloggersService,
    @inject(PostsService) protected postsService: PostsService,
  ) {
  }

  async findAll(req: Request, res: Response) {
    const params = {
      pageNumber: Number(req.query.pageNumber) || 1,
      pageSize: Number(req.query.pageSize) || 10,
      sortBy: req.query.sortBy,
      sortDirection: req.query.sortDirection,
      ...(req.query.searchNameTerm && { searchNameTerm: String(req.query.searchNameTerm) || undefined }),
    };
    // @ts-ignore
    const items = await this.bloggersService.findAllWithPagination(params);
    res.status(200).send(items);
  }

  async create(req: Request, res: Response) {
    const name = req.body.name;
    const websiteUrl = req.body.websiteUrl;
    const description = req.body.description;
    const newItem = await this.bloggersService.create({ name, websiteUrl, description });
    res.status(201).send(newItem);
  }

  async findOne(req: Request, res: Response) {
    const item = await this.bloggersService.findOne({ id: req.params.id });
    if (!item) return res.send(404);
    res.send(item);
  }

  async update(req: Request, res: Response) {
    const fields = {
      ...(req.body.name && { name: req.body.name }),
      ...(req.body.websiteUrl && { websiteUrl: req.body.websiteUrl }),
    };
    const isUpdated = await this.bloggersService.update({
      id: req.params.id,
      ...fields
    });
    if (!isUpdated) return res.send(404);
    res.send(204);
  }

  async remove(req: Request, res: Response) {
    const isRemoved = await this.bloggersService.remove({ id: req.params.id });
    if (!isRemoved) return res.send(404);
    res.send(204);
  }

  async findAllPosts(req: Request, res: Response) {
    const item = await this.bloggersService.findOne({ id: req.params.id });
    if (!item) return res.send(404);
    const params = {
      blogId: req.params.id,
      pageNumber: Number(req.query.pageNumber) || 1,
      pageSize: Number(req.query.pageSize) || 10,
      sortBy: req.query.sortBy,
      sortDirection: req.query.sortDirection,
    };
    // @ts-ignore
    const items = await this.postsService.findAllByBlogId(params);
    res.send(items);
  }

  async createPost(req: Request, res: Response) {
    const item = await this.bloggersService.findOne({ id: req.params.id });
    if (!item) return res.send(404);
    const newItem = await this.postsService.create({
      title: req.body?.title,
      content: req.body?.content,
      shortDescription: req.body?.shortDescription,
      blogName: req.body?.blogName,
      blogId: req.params.id
    });
    res.status(201).send(newItem);
  }
}