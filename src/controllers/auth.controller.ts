import { Request, Response } from 'express';
import { injectable, inject } from 'inversify';
import { AuthService } from '../domain/auth.service';
import { JwtService } from '../application/jwt.service';
import { UsersService } from '../domain/users.service';
import { randomUUID } from 'crypto';
import { RefreshTokenType } from '../repositories/types';
import { addSeconds } from 'date-fns';

@injectable()
export class AuthController {
  constructor(
    @inject(AuthService) protected authService: AuthService,
    @inject(JwtService) protected jwtService: JwtService,
    @inject(UsersService) protected usersService: UsersService,
  ) {
  }

  async registration(req: Request, res: Response) {
    const newUser = await this.authService.createUser(req.body.login, req.body.email, req.body.password);

    if (!newUser) return res.sendStatus(400);

    res.sendStatus(204);
  }

  async login(req: Request, res: Response) {
    const user = await this.authService.checkCredentials(req.body.loginOrEmail, req.body.password);

    if (!user) return res.sendStatus(401);

    const token = this.jwtService.createJWT(user.id);
    const refreshToken: RefreshTokenType = {
      refreshToken: this.jwtService.createRefreshToken(user.id),
      expiresIn: addSeconds(new Date(), 20),
    };
    await this.usersService.addRefreshToken(user.id, refreshToken);

    res.cookie('refreshToken', refreshToken.refreshToken, {
      httpOnly: true,
      secure: true,
    });

    return res.send({ accessToken: token });
  }

  async me(req: Request, res: Response) {
    const user = {
      email: req.user?.accountData.email,
      login: req.user?.accountData.login,
      userId: req.user?.id,
    };
    res.send(user);
  }

  async registrationConfirmation(req: Request, res: Response) {
    const isConfirmed = await this.authService.confirmEmail(req.body.code);

    if (!isConfirmed) return res.sendStatus(400);

    res.sendStatus(204);
  }

  async registrationEmailResending(req: Request, res: Response) {
    const isResend = await this.authService.resendEmail(req.body.email);

    if (!isResend) return res.sendStatus(400);

    res.sendStatus(204);
  }

  async refreshToken(req: Request, res: Response) {
    const refreshTokenFromCookies = req.cookies.refreshToken;

    if (!refreshTokenFromCookies) return res.sendStatus(401);

    const userId = await this.jwtService.getUserIdByRefreshToken(refreshTokenFromCookies);

    if (!userId) return res.sendStatus(401);

    const user = await this.usersService.findOne({ id: userId });

    if (!user || user.blockedRefreshTokens.includes(refreshTokenFromCookies)) {
      return res.sendStatus(401);
    }

    const token = this.jwtService.createJWT(userId);
    const newRefreshToken: RefreshTokenType = {
      refreshToken: this.jwtService.createRefreshToken(userId),
      expiresIn: addSeconds(new Date(), 20),
    };
    await this.usersService.addRefreshToken(userId, newRefreshToken);
    await this.usersService.addBlockedRefreshToken(userId, refreshTokenFromCookies);
    await this.usersService.removeRefreshToken(userId, refreshTokenFromCookies);

    res.cookie('refreshToken', newRefreshToken.refreshToken, {
      httpOnly: true,
      secure: true,
    });

    return res.send({ accessToken: token });
  }

  async logout(req: Request, res: Response) {
    const refreshTokenFromCookies = req.cookies.refreshToken;

    if (!refreshTokenFromCookies) return res.sendStatus(401);

    const userId = await this.jwtService.getUserIdByRefreshToken(refreshTokenFromCookies);

    if (!userId) return res.sendStatus(401);

    const user = await this.usersService.findOne({ id: userId });

    if (!user || user.blockedRefreshTokens.includes(refreshTokenFromCookies)) {
      return res.sendStatus(401);
    }

    await this.usersService.addBlockedRefreshToken(userId, refreshTokenFromCookies);
    await this.usersService.removeRefreshToken(userId, refreshTokenFromCookies);

    return res.sendStatus(204);
  }
}