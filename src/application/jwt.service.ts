import { IUser } from '../repositories/types';
import jwt from 'jsonwebtoken';
import { settings } from '../settings';
import { injectable } from 'inversify';

@injectable()
export class JwtService {
  createJWT(userId: string) {
    return jwt.sign({ userId }, settings.JWT_SECRET, { expiresIn: '10s' });
  }
  createRefreshToken(userId: string) {
    return jwt.sign({ userId }, settings.REFRESH_TOKEN_SECRET, { expiresIn: '20s' });
  }
  getUserIdByAccessToken(token: string) {
    try {
      const result: any = jwt.verify(token, settings.JWT_SECRET);
      return result.userId;
    } catch (error) {
      return null;
    }
  }
  getUserIdByRefreshToken(token: string) {
    let result: any;
    try {
      jwt.verify(token, settings.REFRESH_TOKEN_SECRET, (err, data) => {
        if (err) {
          result = null;
        } else {
          result = data;
        }
      });

      return result?.userId;
    } catch (error) {
      return null;
    }
  }
}